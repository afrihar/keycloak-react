import React, { Component } from 'react';
import UserInfo from './UserInfo';
import Logout from './Logout';
import QueryAPI from './QueryAPI';
import Keycloak from 'keycloak-js';
import PageMain from "./components/Page/PageMain.react";

class Secured extends Component {

    constructor(props) {
        super(props);
        this.state = { keycloak: null, authenticated: false };
    }

    componentDidMount() {
        const keycloak = Keycloak('/keycloak.json');
        keycloak.init({onLoad: 'login-required', checkLoginIframe: false}).then(authenticated => {
            this.setState({ keycloak: keycloak, authenticated: authenticated })
        })
    }

    render() {
        if(this.state.keycloak) {
            if(this.state.authenticated) return (
                <PageMain>
                    <div>
                        <p>
                            User Profile Page (Keycloak Auth)
                        </p>
                        <UserInfo keycloak={this.state.keycloak}/>
                        <QueryAPI keycloak={this.state.keycloak}/>
                        <Logout keycloak={this.state.keycloak}/>
                    </div>
                </PageMain>
            ); else return (<div>Unable to authenticate!</div>)
        }
        return (
            <div>Initializing Keycloak...</div>
        );
    }
}

export default Secured;