import React, {Component} from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import Welcome from './Welcome';
import Secured from './Secured';
import Dashboard from "./views/Dashboard";
import "tabler-react/dist/Tabler.css"
import './App.css';
import PageContent from "./components/PageContent.react"
import SiteWrapper from "./components/SiteWrapper.react";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <SiteWrapper>
                    <PageContent>
                        <Route exact path="/" component={Welcome} />
                        <Route exact path="/dashboard" component={Dashboard} />
                        <Route path="/secured" component={Secured} />
                    </PageContent>
                </SiteWrapper>


            </BrowserRouter>
        );
    }
}

export default App;